"use strict";

import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import {createReply} from "./backend-service";
import Button from '@material-ui/core/Button';
import PostEditor from "./PostEditor";
import PropTypes from 'prop-types';

/**
 *
 * @param {Post} post
 */
function procureState(post) {
    return function () {
        let personObject = post.personData;
        let name = personObject.firstName + " " + personObject.lastName;
        let range = name.length;

        return {
            "blocks": [
                {
                    "key": "70b4q",
                    "text": name + " ",
                    "type": "unstyled",
                    "depth": 0,
                    "inlineStyleRanges": [],
                    "entityRanges": [
                        {
                            "offset": 0,
                            "length": range,
                            "key": 0
                        }
                    ],
                    "data": {}
                }
            ],
            "entityMap": {
                "0": {
                    "type": "mention",
                    "mutability": "SEGMENTED",
                    "data": {
                        "mention": personObject
                    }
                }
            }
        }
    }
}

/**
 * Dialog with action buttons. The actions are passed in as an array of React objects,
 * in this example [FlatButtons](/#/components/flat-button).
 *
 * You can also close this dialog by clicking outside the dialog, or with the 'Esc' key.
 */
export default class ReplyButton extends React.Component {
    state = {
        open: false,
    };

    /**
     *
     * @param {{id, routing, parent: Post}} props
     */
    constructor(props) {
        super(props);

        this.id = props.id;
        this.routing = props.routing;

        /**
         * @type {PostEditor}
         */
        this.editor = undefined;
    }

    handleOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    handleReply = () => {
        this.setState({open: false});
        let contents = this.editor.getContents();

        createReply(contents.text, contents.raw, this.id, this.routing);
    };

    render() {

        const textField = <PostEditor
            ref={(ref) => this.editor = ref}
            placeholder="Write the reply"
            startState={this.props.withName && procureState(this.props.parent)}
        >
        </PostEditor>;

        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onClick={this.handleClose}
            />,
            <FlatButton
                label="Reply"
                primary={true}
                onClick={this.handleReply}
            />
        ];


        return (
            <div>
                <Button size="small" color="primary" onClick={this.handleOpen}>
                    Reply
                </Button>
                <Dialog
                    title="Reply"
                    actions={actions}
                    modal={false}
                    open={this.state.open}
                    onRequestClose={this.handleClose}
                >
                    {textField}
                </Dialog>
            </div>
        );
    }
}

ReplyButton.propTypes = {
    // You can declare that a prop is a specific JS primitive. By default, these
    // are all optional.
    id: PropTypes.string,
    routing: PropTypes.string,
    parent: PropTypes.any,
    withName: PropTypes.bool
};