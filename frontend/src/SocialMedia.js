"use strict";

import React from 'react';
import {Card, CardText, List, RaisedButton} from "material-ui";
import firebase from 'firebase';
import axios from 'axios';
import cookie from 'react-cookies';

// Initialize Firebase
let config = {
    apiKey: "AIzaSyDIfrPPmNX9ZrgLnr3XEHpnTK-PiArdUXs",
    authDomain: "analasis-74714.firebaseapp.com",
    databaseURL: "https://analasis-74714.firebaseio.com",
    projectId: "analasis-74714",
    storageBucket: "analasis-74714.appspot.com",
    messagingSenderId: "959245874988"
};

firebase.initializeApp(config);

/**
 * Dialog with action buttons. The actions are passed in as an array of React objects,
 * in this example [FlatButtons](/#/components/flat-button).
 *
 * You can also close this dialog by clicking outside the dialog, or with the 'Esc' key.
 */
export default class SocialMedia extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            facebook: props.facebook || cookie.load("facebook", true),
            twitter: props.twitter || cookie.load("twitter", true)
        };

    }

    initializeFacebook = () => {
        var provider = new firebase.auth.FacebookAuthProvider();
        provider.addScope('user_birthday');
        provider.addScope('publish_actions');
        provider.addScope('pages_messaging');
        provider.addScope('manage_pages');
        provider.addScope('publish_actions');
        provider.addScope('publish_pages');

        provider.addScope('email');
        provider.addScope('user_age_range');
        provider.addScope('user_friends');
        provider.addScope('user_gender');
        provider.addScope('user_hometown');
        provider.addScope('user_link');
        provider.addScope('user_location');

        provider.addScope('user_posts');

        firebase.auth().signInWithPopup(provider).then((result) => {
            // This gives you a Facebook Access Token. You can use it to access the Facebook API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;

            axios.put("/bknd/facebookInfo", {token}).then((result) => {
                if (result.status === 200) {
                    cookie.save("facebook", true);
                    this.setState({facebook: true});
                }
                else console.error("Server responded with " + result.status);
            }, (error) => {
                console.error(error);
            });


        }).catch(function (error) {
            console.error(error);
        });

    };

    initializeTwitter = () => {
        var provider = new firebase.auth.TwitterAuthProvider();

        firebase.auth().signInWithPopup(provider).then((result) => {
            // This gives you a the Twitter OAuth 1.0 Access Token and Secret.
            // You can use these server side with your app's credentials to access the Twitter API.
            var token = result.credential.accessToken;
            var secret = result.credential.secret;
            // The signed-in user info.
            var user = result.user;

            var id = result.additionalUserInfo.profile.id_str;

            axios.put("/bknd/twitterInfo", {token, secret, id}).then((result) => {
                if (result.status === 200) {
                    cookie.save("twitter", true);
                    this.setState({twitter: true});
                }
                else console.error("Server responded with " + result.status);
            }, (error) => {
                console.error(error);
            });

        }).catch(function (error) {
            console.error(error);
        });

    };

    render() {
        let facebookProps = {
            label: "Facebook"
        };
        let twitterProps = {
            label: "Twitter"
        };
        console.log(this.state);
        if (!this.state.facebook)
            facebookProps.buttonStyle = {backgroundColor: 'red'};
        if (!this.state.twitter)
            twitterProps.buttonStyle = {backgroundColor: 'red'};
        return (
            <div>

                <RaisedButton
                    {...facebookProps}
                    primary={true}
                    onClick={this.initializeFacebook}
                />
                <RaisedButton
                    {...twitterProps}
                    primary={true}
                    onClick={this.initializeTwitter}
                />

            </div>

        );
    }
}