import React, {Component} from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import io from 'socket.io-client';

import NewPost from "./NewPost";
import Post from "./Post";
import {AppBar, List, RaisedButton} from "material-ui";
import SocialMedia from "./SocialMedia";
import {getPosts, getReach, setSocket} from "./backend-service";
import PostList from "./PostList";
import Stats from "./Stats";

/**
 *
 * @param {PostData} postData
 */
function doesNotHaveId(postData) {
    let ret = !this.has(postData.id);
    this.add(postData.id);
    return ret;
}

class Main extends Component {
    constructor(props) {
        super(props);
        this.socket = undefined;
        this.state = {
            /**
             * @type Post[]
             */
            posts: []
        };

        this.processedIds = new Set();
        /**
         *
         * @type {Map<string, {sort, post: Post}>}
         */
        this.posts = new Map();
        this.logout = props.logout;

        /**
         * @type PostList
         */
        this.postsList = undefined;

        this.state.postsList = <PostList parent={this} allPosts={this.posts}/>;

        // connect to ws
        var socket = io('/');
        socket.on('connection', () => {
            console.log("Connected")
        });
        socket.on('new/post', this.handlePost);
        socket.on('new/reply', this.handleReply);
        socket.on('news', this.handleNews);

        socket.on('message', function (data) {
            console.log(data);
        });

        setSocket(socket);

        this.socket = socket;
    }

    handlePost = (data) => {

        console.log("new post");
        let payload = data.payload;

        if (doesNotHaveId.call(this.processedIds, payload))
            this.postsList.addPost(payload, true);
        else return console.log("Already have this post...");
    };


    handleReply = (data) => {
        console.log("New reply");
        console.log(data);

        /**
         * @type ReplyData
         */
        let payload = data.payload;

        if (!doesNotHaveId.call(this.processedIds, payload)) {
            return console.log("Already have this post...");
        }

        console.log(this.posts);
        let parent = this.posts.get(payload.type.parent);
        // todo get the parent if it's missing

        parent.addReply(payload);
    };


    handleNews = (data) => {
        console.log(data);
        this.socket.emit('message', {my: 'data'});
    };

    componentDidMount() {
        // get recent posts

        getPosts((err, data) => {
            if (err) {
                console.error(err);
                return setTimeout(this.componentDidMount.bind(this), 50);
            }

            this.postsList.addPosts(data.filter(doesNotHaveId, this.processedIds));
        });

        getReach((err, data) => {
            if (err) {
                console.error(err);
                return setTimeout(this.componentDidMount.bind(this), 50);
            }

            this.setState({
                reach: data
            })
        });
    }


    render() {
        let r;
        if (this.state.reach) {
            r = <div>
                🤝 {this.state.reach}
            </div>
        }
        return (
            <MuiThemeProvider>
                <div style={{height: '100%'}} className="main" key="main">
                    <AppBar title="Analysis"
                            iconElementLeft={
                                <div style={{display: "inline-flex"}}>
                                    <RaisedButton onClick={this.logout}>
                                        Logout
                                    </RaisedButton>
                                    <div style={{
                                        'margin-left': "10px",
                                        'margin-right': "10px",
                                        'margin-top': '5px'
                                    }}>

                                        {r}
                                    </div>

                                    <Stats/>
                                </div>
                            }>
                        <SocialMedia/>
                    </AppBar>
                    <div className="main-list">
                        {this.state.postsList}
                    </div>
                    <NewPost socket={this.socket}/>
                </div>
            </MuiThemeProvider>
        );
    }
}


export default Main;
