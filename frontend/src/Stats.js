"use strict";

import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {getTimeseries} from "./backend-service";
import PostEditor from "./PostEditor";


import {Line} from 'react-chartjs-2';

/**
 * Dialog with action buttons. The actions are passed in as an array of React objects,
 * in this example [FlatButtons](/#/components/flat-button).
 *
 * You can also close this dialog by clicking outside the dialog, or with the 'Esc' key.
 */
export default class Stats extends React.Component {
    state = {
        open: false,
    };

    constructor(props) {
        super(props);

    }

    handleOpen = () => {
        getTimeseries((err, timeseries) => {
            this.setState({open: false});

            if (err)
                return console.error(err);

            timeseries = timeseries.reverse();

            var hours = timeseries.map(t => new Date(t.createdDate).getHours());
            let datasets = [{
                label: 'Reach fluctuations',
                fill: false,
                lineTension: 0.1,
                backgroundColor: 'rgba(75,192,192,0.4)',
                borderColor: 'rgba(75,192,192,1)',
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: 'rgba(75,192,192,1)',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: timeseries.map(t => t.reach)
            }];

            const data = {
                labels: hours,
                datasets
            };

            this.setState({
                open: true,
                data
            });
        });
    };

    handleClose = () => {
        this.setState({open: false});
    };

    render() {
        if (this.state.data)
            var graph = <Line data={this.state.data} options={{
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }}/>;

        const actions = [
            <FlatButton
                label="Close"
                primary={true}
                onClick={this.handleClose}
            />
        ];


        return (
            <div>
                <RaisedButton label="Stats" onClick={this.handleOpen}/>
                <Dialog
                    title="Stats"
                    actions={actions}
                    modal={false}
                    open={this.state.open}
                    onRequestClose={this.handleClose}
                >
                    {graph}
                </Dialog>
            </div>
        );
    }
}