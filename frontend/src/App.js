import React, {Component} from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';
import cookie from 'react-cookies';
// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

import './App.css';
import LoginScreen from './Loginscreen';
import Main from "./Main";
import PostEditor from "./PostEditor";



class App extends Component {
    constructor(props) {
        super(props);

        this.makeLogOut = () => {
            cookie.remove("cookie");
            this.setState({
                loggedIn: false,
                socialNetworksAuth: {
                    facebook: false,
                    twitter: false
                }
            })
        };

        this.state = {
            loggedIn: !!cookie.load("cookie", true),
            socialNetworksAuth: {
                facebook: !!cookie.load("facebook", true),
                twitter: !!cookie.load("twitter", true)
            }
        };
    }

    makeLogIn() {
        this.setState({
            loggedIn: true,
            socialNetworkAuth: {
                facebook: !!cookie.load("facebook", true),
                twitter: !!cookie.load("twitter", true)
            }
        })
    }

    render() {

        if (this.state.loggedIn) {
            return (<div className="App">
                {<Main logout={this.makeLogOut}/>}
            </div>);
        }
        else {
            return (
                <div className="App">
                    <LoginScreen appContext={this}/>
                </div>
            );
        }
    }
}

export default App;
