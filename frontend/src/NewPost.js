"use strict";

import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {createPost} from "./backend-service";
import PostEditor from "./PostEditor";


/**
 * Dialog with action buttons. The actions are passed in as an array of React objects,
 * in this example [FlatButtons](/#/components/flat-button).
 *
 * You can also close this dialog by clicking outside the dialog, or with the 'Esc' key.
 */
export default class NewPost extends React.Component {
    state = {
        open: false,
    };

    constructor(props) {
        super(props);

        /**
         *
         * @type {PostEditor}
         */
        this.editor = undefined;
    }

    handleOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    handlePost = () => {
        this.setState({open: false});

        let contents = this.editor.getContents();

        createPost(contents.text, contents.raw);
    };

    render() {
        const textField = <PostEditor
            ref={(ref) => this.editor = ref}
            placeholder="Write the reply"
        >
        </PostEditor>;

        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onClick={this.handleClose}
            />,
            <FlatButton
                label="Post"
                primary={true}
                onClick={this.handlePost}
            />,
        ];


        return (
            <div>
                <RaisedButton label="New Post" onClick={this.handleOpen}/>
                <Dialog
                    title="New post"
                    actions={actions}
                    modal={false}
                    open={this.state.open}
                    onRequestClose={this.handleClose}
                >
                    {textField}
                </Dialog>
            </div>
        );
    }
}