"use strict";

import React from 'react';
import {AppBar, List, RaisedButton} from "material-ui";
import Post from "./Post";


/**
 * Dialog with action buttons. The actions are passed in as an array of React objects,
 * in this example [FlatButtons](/#/components/flat-button).
 *
 * You can also close this dialog by clicking outside the dialog, or with the 'Esc' key.
 * @property {PostData} state
 */
export default class PostList extends React.Component {
    /**
     *
     * @param {PostData} props
     */
    constructor(props) {
        super(props);

        this.state = {
            posts: []
        };

        this.level = props.level || 0;

        this.allPosts = props.allPosts;
        this.parent = props.parent;

        this.parent.postsList = this;
        this.maxCreatedDate = 0;

        if (props.posts)
            this.addPosts(props.posts);
    }

    render() {

        return (
            <List className="post-list">
                {this.state.posts}
            </List>
        );
    }

    _addPost(post) {
        post = this.postify(post);
        let posts = this.state.posts;
        if (post.props.createdDate > this.maxCreatedDate)
            this.maxCreatedDate = post.props.createdDate;
        posts.unshift(post);
    }

    addPost(post) {
        this._addPost(post);
        this.reorderPosts();
    }

    _addPosts(posts) {
        posts.forEach(this._addPost, this);

        this.reorderPosts();

    }

    addPosts(posts) {

        this._addPosts(posts);
        this.reorderPosts();

    }

    reorderPosts = () => {
        let order = this.state.posts.sort(Post.comparator);

        if (order[0].props.sort.value !== this.maxCreatedDate) {
            this.maxCreatedDate = order[0].props.sort.value;

            if (this.parent.reorderPosts)
                this.parent.reorderPosts();
        }
        this.setState({
            posts: order
        });

    };


    /**
     *
     * @param {PostData} postData
     * @return {{post: Post, sort}}
     */
    postify(postData) {
        let post = (
            <Post {...postData} key={postData.id} allPosts={this.allPosts} sort={{value: postData.createdDate}}
                  reorderPosts={this.reorderPosts} level={this.level}/>);

        return post;
    }
}



