import axios from 'axios';

const backendUrl = "/bknd/";

let userId, socket;

export function setUserId(userId_) {
    userId = userId_;
}

let commands = new Map();

export function setSocket(socket_) {
    socket = socket_;

    socket.on('outcome', (data) => {
        let command = commands.get(data.nonce);
        if (!command) {
            console.error("Received unknown response.");
            console.error(command);
        }
        else {
            command(data.error, data.outcome);
        }
    });
}

const get = 'get';
const endPoints = {
    post: "post",
    reach: "reach",
    timeseries: "timeseries",
    person: "person"
};

for (let endpoint in endPoints) {
    endPoints[endpoint] = backendUrl + endPoints[endpoint];
}


/**
 *
 * @param ids
 * @param {function(err, Object<string, PersonData>)} callback
 */
export function getPersons(ids, callback) {

    let params = {
        ids
    };
    axios.get(endPoints.person, {params}).then((response) => {
        let data = response.data;

        callback(null, data);
    }, callback);

}

/**
 *
 * @param search
 * @param {function(err, PersonData[])} callback
 */
export function searchPerson(search, callback) {

    let params = {
        search
    };
    axios.get(endPoints.person + "/_search", {params}).then((response) => {
        let data = response.data;

        callback(null, data);
    }, callback);

}

/**
 *
 * @param [from]
 * @param [count]
 * @param [replySize]
 * @param {function(err, PostData[])} callback
 */
export function getPosts(callback, from, count, replySize) {

    let params = {
        from,
        count,
        replySize
    };
    axios.get(endPoints.post, {params}).then((response) => {
        let data = response.data;

        callback(null, data);
    }, callback);

}

/**

 * @param {function(err, number)} callback
 */
export function getReach(callback) {

    axios.get(endPoints.reach, {}).then((response) => {
        let data = response.data;

        callback(null, data && data.reach);
    }, callback);

}

/**

 * @param {function(err, {})} callback
 */
export function getTimeseries(callback) {

    var start = new Date();
    start.setHours(0,0,0,0);


    axios.get(endPoints.timeseries, {start:start}).then((response) => {
        let data = response.data;

        callback(null, data);
    }, callback);

}

function commandLogger(err, outcome) {
    if (err) {
        console.error("Received an error on command " + outcome.nonce);
        console.error(err);
    }
    else console.log("Successfully ran a command");

}

function getPayload(callback = commandLogger) {
    let nonce = Math.random();
    commands.set(nonce, callback);
    return {
        nonce,
        payload: undefined
    }
}

export function createPost(content, raw, callback) {
    let message = getPayload(callback);

    message.payload = {content, raw};

    socket.emit('create/post', message);
}

export function createReply(content, raw, id, routing, callback) {
    let message = getPayload(callback);

    message.payload = {
        content,
        raw,
        id,
        routing
    };

    socket.emit('create/reply', message);
}