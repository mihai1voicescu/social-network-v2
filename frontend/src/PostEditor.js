import React, {Component} from 'react';
import Editor from 'draft-js-plugins-editor';
import createLinkifyPlugin from 'draft-js-linkify-plugin';
import createMentionPlugin from 'draft-js-mention-plugin';
import createEmojiPlugin from 'draft-js-emoji-plugin';
import {EditorState, convertToRaw, convertFromRaw} from 'draft-js';
import 'draft-js/dist/Draft.css';

import 'draft-js-mention-plugin/lib/plugin.css'

import 'draft-js-emoji-plugin/lib/plugin.css';

import './PostEditor.css';

import PropTypes from 'prop-types';
import {searchPerson} from "./backend-service";


// const hashtagPlugin = createHashtagPlugin();
const linkifyPlugin = createLinkifyPlugin();

// Creates an Instance. At this step, a configuration object can be passed in
// as an argument.
const emojiPlugin = createEmojiPlugin({
    // imagePath: "/emoji/",
    allowImageCache: true
});
const {EmojiSuggestions, EmojiSelect} = emojiPlugin;

const plugins = [
    linkifyPlugin,
    emojiPlugin,
];

export default class PostEditor extends Component {

    constructor(props) {
        super(props);

        let editorState;

        if (props.startState)
            editorState = EditorState.createWithContent(convertFromRaw(props.startState()));
        else editorState = EditorState.createEmpty();

        this.state = {
            editorState,
            suggestions: [],
        };


        this.mentionPlugin = createMentionPlugin({
        });

    }

    lastSuccessfulSearchCount = 0;
    lastSearchCount = 0;



    onSearchChange = ({value}) => {
        let current = this.lastSearchCount++;

        searchPerson(value, (err, results) => {
            if (err){
                console.error("Unable to search user.");
                console.error(err);
            }

            else {
                console.log( current, this.lastSuccessfulSearchCount);
                if (current >= this.lastSuccessfulSearchCount) {
                    this.lastSuccessfulSearchCount = current;
                    for (let hit of results) {
                        hit.name = hit.firstName + " " + hit.lastName;
                        if (Array.isArray(hit.avatar))
                            hit.avatar = hit.avatar[0];
                    }
                    this.setState({
                        suggestions: results
                    });
                }
            }
        });

    };

    onChange = (editorState) => {
        this.setState({
            editorState,
        });
    };

    getContents() {
        let content = this.state.editorState.getCurrentContent();
        return {
            raw: convertToRaw(content),
            text: content.getPlainText()
        }
    }

    render() {
        const {MentionSuggestions} = this.mentionPlugin;

        const _plugins = [this.mentionPlugin].concat(plugins);

        return (
            <div className="editor" onClick={this.focus}>
                <Editor
                    placeholder={this.props.placeholder}
                    editorState={this.state.editorState}
                    onChange={this.onChange}
                    plugins={_plugins}
                    ref={(element) => {
                        this.editor = element;
                    }}
                />
                <EmojiSuggestions/>
                <EmojiSelect/>
                <MentionSuggestions
                    onSearchChange={this.onSearchChange}
                    suggestions={this.state.suggestions}
                />
            </div>
        );
    }
}

PostEditor.propTypes = {
    startState: PropTypes.func,
    placeholder: PropTypes.string
};