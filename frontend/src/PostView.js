import React, {Component} from 'react';
import Editor from 'draft-js-plugins-editor';
import createLinkifyPlugin from 'draft-js-linkify-plugin';
import createMentionPlugin from 'draft-js-mention-plugin';
import createEmojiPlugin from 'draft-js-emoji-plugin';
import {EditorState, convertFromRaw, convertFromHTML, ContentState} from 'draft-js';
import 'draft-js/dist/Draft.css';

import 'draft-js-mention-plugin/lib/plugin.css'

import 'draft-js-emoji-plugin/lib/plugin.css';

import './PostView.css';

// const hashtagPlugin = createHashtagPlugin();
const linkifyPlugin = createLinkifyPlugin();

// Creates an Instance. At this step, a configuration object can be passed in
// as an argument.
const emojiPlugin = createEmojiPlugin({
    // imagePath: "/emoji/",
    allowImageCache: true
});

const plugins = [
    linkifyPlugin,
    emojiPlugin,
];

export default class PostView extends Component {

    constructor(props) {
        super(props);


        let editorState;
        if (props.raw)
            editorState = EditorState.createWithContent(convertFromRaw(props.raw));
        else if (props.text)
            editorState = EditorState.createWithContent(ContentState.createFromText(props.text));
        else if (props.html) {
            let blocksFromHTML = convertFromHTML(props.html);
            editorState = EditorState.createWithContent(ContentState.createFromBlockArray(
                blocksFromHTML.contentBlocks,
                blocksFromHTML.entityMap
            ));
        }

        else {
            console.error("Created PostView without content.");
            editorState = EditorState.createEmpty();
        }

        this.mentionPlugin = createMentionPlugin({});

        console.log(editorState);
        this.state = {editorState};

    }

    onChange = (editorState) => {
        this.setState({
            editorState,
        });
    };




    render() {
        const {MentionSuggestions} = this.mentionPlugin;

        const _plugins = [this.mentionPlugin].concat(plugins);

        return (
            <div className="PostView" onClick={this.focus}>
                <Editor
                    editorState={this.state.editorState}
                    onChange={this.onChange}
                    readOnly={true}
                    plugins={_plugins}
                    ref={(element) => {
                        this.editor = element;
                    }}
                />
            </div>
        );
    }
}