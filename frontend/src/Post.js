"use strict";

import React from 'react';
import {getPersons} from "./backend-service";


import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CardHeader from '@material-ui/core/CardHeader';
import ReplyButton from "./ReplyButton";
import PostList from "./PostList";
import PostView from "./PostView";

import {Doughnut} from 'react-chartjs-2';


const SPLIT_RE = /(.+?)(\[@.+?|.+?])|.+?/g;

function procureData(positivity) {
    let color, label;

    if (positivity < 0) {
        positivity = -positivity;
        label = 'Negativity';
        color = '#000000';
    }
    else {
        label = 'Positivity';
        color = '#FF6384';
    }

    console.log("FINDME");
    console.log({
        labels: [
            label,
            'Absent'
        ],
        datasets: [{
            data: [positivity, 1 - positivity],
            backgroundColor: [
                color,
                '#ffffff'
            ],
            hoverBackgroundColor: [
                color,
                '#ffffff'
            ]
        }]
    });

    return {
        labels: [
            label,
            'Absent'
        ],
        datasets: [{
            data: [positivity, 1 - positivity],
            backgroundColor: [
                color,
                '#ffffff'
            ],
            hoverBackgroundColor: [
                color,
                '#ffffff'
            ]
        }]
    }
}

const data = {
    labels: [
        'Positivity',
        'Green'
    ],
    datasets: [{
        data: [300, 50],
        backgroundColor: [
            '#FF6384',
            '#ffffff'
        ],
        hoverBackgroundColor: [
            '#FF6384',
            '#ffffff'
        ]
    }]
};
/**
 * Dialog with action buttons. The actions are passed in as an array of React objects,
 * in this example [FlatButtons](/#/components/flat-button).
 *
 * You can also close this dialog by clicking outside the dialog, or with the 'Esc' key.
 * @property {PostData} state
 */
export default class Post extends React.Component {
    /**
     *
     * @param {PostData} props
     */
    constructor(props) {
        super(props);

        let state = {};
        Object.assign(state, props);
        state.key = state.id;

        this.routing = props.thread;

        this.level = props.level || 0;


        if (this.level > 1) {
            this.withName = true;
            this.replyId = props.type.parent;
        }
        else {

            this.replyId = props.id;
        }

        this.sort = props.sort;
        this.reorderPosts = props.reorderPosts;

        // state.content = Post.replacePlaceholders(state.content);

        /**
         * @type PostData
         */
        this.state = state;

        let sync = true;
        let personId = props.personId || props.userId;

        getPersons(personId, (err, result) => {
            if (err || !result) {
                console.error("Unable to get person info.");
                if (err)
                    console.error(err);
            }
            else {
                /**
                 *
                 * @type {PersonData}
                 */
                let personData = result[personId];
                if (personData) {
                    /**
                     * @type {PersonData}
                     */
                    this.personData = personData;
                    if (sync) {
                        this.state.name = personData.firstName + " " + personData.lastName;
                        this.state.avatar = personData.avatar;
                        this.state.followers = personData.metrics && personData.metrics.followers;
                    }
                    else
                        this.setState({
                            avatar: personData.avatar,
                            name: personData.firstName + " " + personData.lastName,
                            followers: personData.metrics && personData.metrics.followers

                        });
                }
            }
        });

        sync = false;


        this.allPosts = props.allPosts;

        /**
         * @PostList
         */
        this.postsList = undefined;

        this.state = state;

        this.state.postsList = (
            <PostList posts={props.children} parent={this} allPosts={this.allPosts} level={this.level + 1}/>);

        if (this.postsList) {
            if (this.postsList.maxCreatedDate > this.postsList) {
                this.sort.value = this.postsList.maxCreatedDate;
                this.reorderPosts();
            }
        }

        console.log(props.sort);
        console.log(this.sort);
    }

    componentDidMount() {
        if (this.allPosts)
            this.allPosts.set(this.state.id, this);
    }

    componentWillUnmount() {
        if (this.allPosts)
            this.allPosts.delete(this.state.id);
    }


    /**
     *
     * @param {ReplyData} replyData
     */
    addReply(replyData) {
        this.postsList.addPost(replyData);
    }

    static replacePlaceholders(content) {
        let parts = [];
        SPLIT_RE.lastIndex = 0;
        let match;

        while ((match = SPLIT_RE.exec(content))) {
            if (match[1]) {
                parts.push(match[1], (<a href={"/person/" + match[2]}>{match[3]}</a>));
            }
            else parts.push(match[0]);
        }


        return parts.length === 1 ? parts[0] : parts;
    }

    render() {
        if (this.state.metrics && this.state.metrics.likes)
            var interactions = <div>
                {this.state.metrics.likes + " ❤️"}
            </div>;
        let title = (<Typography gutterBottom variant="headline" component="h2">
            {this.state.name || "Post"} {this.state.followers && ("🤝 " + this.state.followers)}
        </Typography>);
        let avatar = (<img src={this.state.avatar || "/logo.svg"} className="avatar"/>);

        let positivityElement;

        let text = "";
        if (this.props.positivity != null) {
            let pos = this.props.positivity;
            if (pos < 0) {
                text = Math.floor((-pos) * 100) + "% ☹️";
            }
            else if (pos === 0) {
                text = "😐"
            }
            else text = Math.floor((pos) * 100) + "% 😀";

            if (text.length !== 6) {
                text = "\u00a0".repeat(6 - text.length) + text;
            }

            text = <div>
                {text}
            </div>;

            // positivityElement = <div className="positivity"><Doughnut className="positivity" options={{
            //     tooltips: {enabled: false}
            // }} legend={{display: false}} data={procureData(this.props.positivity)}/></div>;
        }


        return (<Card className='Post'>
            <CardHeader
                avatar={avatar}
                title={title}
            />
            <CardContent>
                <PostView raw={this.state.raw} text={this.state.content}/>
            </CardContent>
            <CardActions>
                <ReplyButton id={this.replyId} parent={this} routing={this.routing} withName={this.withName}/>
                {text}
                {interactions}
            </CardActions>
            <div className="Replies">
                {this.state.postsList}
            </div>
        </Card>);
    }

    static comparator(a, b) {
        return b.props.sort.value - a.props.sort.value;
    }
}


