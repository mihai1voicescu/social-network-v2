DROP SCHEMA analysis;
CREATE SCHEMA analysis;
USE analysis;

CREATE TABLE user (
  id       INT PRIMARY KEY AUTO_INCREMENT,
  user     VARCHAR(64),
  password VARCHAR(64),
  twitter_token TEXT,
  twitter_secret TEXT,
  facebook_token TEXT,
  facebook_secret TEXT,
  facebook_page_token TEXT,
  facebook_page_id TEXT,
  created  TIMESTAMP DEFAULT NOW() NOT NULL
);

ALTER TABLE user ADD twitter_id text NULL;

CREATE TABLE cookie (
  cookie  VARCHAR(64),
  user_id INT,

  CONSTRAINT FOREIGN KEY fk_cookie(user_id) REFERENCES user (id)
);
