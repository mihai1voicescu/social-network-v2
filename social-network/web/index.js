"use strict";

require("./extendJsFunctionality");
const express = require("express");
const config = require("../../config");
/**
 *
 * @type {Express}
 */
const app = express();
const jsonParse = express.json();
const data = require("./data");
const {mysql, logger} = data;
const Router = express.Router;
const WebErrors = require("./WebErrors");
const security = require("./security");
const server = require('http').Server(app);
const io = require('socket.io')(server);
const extractors = require("./extractors");
// give cookies
const cookie = require('cookie');
const cook = require('cookie-parser')();
const cluster = require("./cluster");

const AccountInformationCache = require("./AccountInformationCache");

app.use(cook);
app.use(express.json());

const socialNetworks = require("./social-networks");

global.assureEsField = function(field, element) {
    if (Array.isArray(field)) {
        return field.includes(element);
    }
    else return field === element;
};

/**
 *
 * >>> login <<<
 */

app.use("/login", express.static("./login"));

app.get("/makeLogin", function (req, res, next) {
    var query = req.query;
    if (!query)
        return next(new WebErrors.InvalidCredentials());

    mysql.query(/* language=MySQL */ "SELECT * FROM user WHERE user = ? AND password = ?;", [query.username, query.password], (err, rows) => {
        if (err)
            next(err);

        else if (rows.length !== 1) {
            next(new WebErrors.InvalidCredentials())
        }

        else {
            var cookie = security.generateCookie();

            let userInfo = rows[0];

            mysql.query(/* language=MySQL */ "INSERT INTO cookie (cookie, user_id) VALUES (?, ?);", [cookie, userInfo.id], (err) => {
                if (err)
                    return next(err);

                res.cookie('cookie', cookie, {maxAge: 900000, httpOnly: false});
                if (userInfo.facebook_token)
                    res.cookie('facebook', true, {maxAge: 900000, httpOnly: false});
                if (userInfo.twitter_token && userInfo.twitter_secret)
                    res.cookie('twitter', true, {maxAge: 900000, httpOnly: false});

                res.sendStatus(200);
            });
        }
    });
});

/**
 *
 * >>> Error handling <<<
 */

const FALLBACK_ERROR = new WebErrors.InternalServerError();

function auth(req, res, next) {
    let cookie = req.cookies.cookie;
    extractors.getUserInfoByCookie(cookie, (err, userInfo) => {
        if (err)
            return next(err);

        req.user = userInfo;

        next(userInfo ? null : new WebErrors.AuthentificationError("Invalid session."));

    });
}

app.use(function (err, req, res, next) {
    console.error(err);
    next(err);
});

app.use(function (err, req, res, next) {
    if (err instanceof WebErrors.WebError) {
        err.toResponse(res);
    }

    else new WebErrors.InternalServerError(err).toResponse(res);
});

/**
 *
 * >>> backend <<<
 */

const authBknd = new Router();

authBknd.use(auth);
authBknd.post("/post", jsonParse, function (req, res, next) {
    /**
     * @type {Object}
     * @property {string} content
     */
    let data = req.body;

    AccountInformationCache.AccountInformationCache.get(req.user.id,
        /**
         *
         * @param err
         * @param {User} user
         */
        (err, user) => {
            if (err)
                return reportError(this, data, err);

            user.postGlobal(data.content, data.raw, data.photos, (err, meta) => {
                if (err)
                    return next(err);

                res.send(meta);
            });

        });
});

authBknd.put("/twitterInfo", function (req, res, next) {
    var data = req.body;
    if (!data)
        return next(new WebErrors.UnsuportedSelection());

    mysql.query(/* language=MySQL */ "UPDATE user SET twitter_secret = ?, twitter_token = ?, twitter_id = ?  WHERE id = ?;", [data.secret, data.token,data.id, req.user.id], (err, result) => {
        if (err)
            next(err);

        else if (result.affectedRows !== 1) {
            next(new WebErrors.InternalServerError());
        }

        else {
            AccountInformationCache.resetUser(req.user.id);
            res.sendStatus(200);
        }
    });
});

authBknd.get("/post", jsonParse, function (req, res, next) {
    extractors.getPosts(req.user.id, (err, posts) => {
        if (err)
            return next(err);

        res.send(posts);
    }, req.query.from, req.query.count, req.query.replySize);
});


authBknd.get("/reach", jsonParse, function (req, res, next) {
    extractors.getReach(req.user.id, (err, posts) => {
        if (err)
            return next(err);

        res.send({reach: posts});
    });
});
authBknd.get("/timeseries", jsonParse, function (req, res, next) {
    extractors.getTimeseries(req.query.start,req.query.end, req.user.id, (err, timeseries) => {
        if (err)
            return next(err);

        res.send(timeseries);
    });
});

authBknd.get("/checkLogin", function (req, res, next) {
    res.sendStatus(200);
});

authBknd.get("/person", function (req, res, next) {
    let ids = req.query.ids;
    if (!ids)
        return next(new WebErrors.UnsuportedSelection());

    extractors.getPersons(ids, req.user.id, (err, result) => {
        if (err || !result)
            return next(err || new WebErrors.NotFound());

        res.send(result);
    });
});

authBknd.get("/person/_search", function (req, res, next) {
    let search = req.query.search;

    extractors.searchPerson(search, req.user.id, (err, result) => {
        if (err || !result)
            return next(err || new WebErrors.NotFound());

        res.send(result);
    });
});


/**
 *
 * >>> integrations <<<
 */

for (let socialNetwork of Object.values(socialNetworks)) {
    app.use("/webhook/" + socialNetwork.type, socialNetwork.webhooksRouter);
    authBknd.use("/" + socialNetwork.type + "Info", genericNewUserHandler(socialNetwork));
}

function genericNewUserHandler(Class) {
    return function (req, res, next) {
        let data = req.body;

        if (!data)
            return next(new WebErrors.UnsuportedSelection());

        Class.newUser(req.user.id, data, (err) => {
            if (err)
                return next(err);

            res.sendStatus(200);
        });
    };
}

app.use("/bknd", authBknd);

/**
 *
 * >>> socket <<<
 */

io.use(function (socket, next) {
    let data = socket.request;
    let cookies = cookie.parse(data.headers.cookie);

    extractors.getUserInfoByCookie(cookies.cookie, (err, userInfo) => {
        if (err)
            return next(err);

        cluster.addConnection(socket, userInfo);
        next();
    });
});

io.on('connection', function (socket) {
    var userInfo = socket.userInfo;
    socket.userId = userInfo.id;

    cluster.addConnection(socket, socket.userId);
    socket.on('message', function (data) {
        console.log(data);
    });
    socket.on('create/post', createPost);
    socket.on('create/reply', createReply);
});

function createPost(data) {
    let payload = data.payload;
    AccountInformationCache.AccountInformationCache.get(this.userId,
        /**
         *
         * @param err
         * @param {User} user
         */
        (err, user) => {
            if (err)
                return reportError(this, data, err);

            user.postGlobal(payload.content, payload.raw, payload.photos, (err, meta) => {
                if (err)
                    return reportError(this, data, err);
                else reportSuccess(this, data, meta);
            });

        });
}


function createReply(data) {
    let payload = data.payload;
    AccountInformationCache.AccountInformationCache.get(this.userId,
        /**
         *
         * @param err
         * @param {User} user
         */
        (err, user) => {
            if (err)
                return reportError(this, data, err);

            user.reply(payload.id, payload.routing, payload.content, payload.raw, payload.photos, (err, meta) => {
                if (err)
                    return reportError(this, data, err);
                else reportSuccess(this, data, meta);
            });

        });
}


function reportError(socket, data, error) {
    logger.error(new WebErrors.SubsequentError("An error occurred on socket operation.", error));
    socket.emit('outcome', {nonce: data.nonce, error});
}

function reportSuccess(socket, data, response) {
    socket.emit('outcome', {nonce: data.nonce, response});
}

server.listen(config.web.port, config.web.host);
