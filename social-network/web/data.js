"use strict";

const Mysql = require("mysql");
const config = require("../../config");
const ES = require("elasticsearch");
const redisCl = require("redis");
const bunyan = require("bunyan");

const logger = bunyan.createLogger({name: 'analysis'});

const proxy = new Proxy(logger, {
    get: function (logger, prop) {
        if (prop === 'error')
            return (err, message) => {
                console.error(err);
                console.error(message);
                logger.error(err, message);
            };
            else return logger[prop];
    }
});


/**
 *
 * @type {FBSDK}
 */
const FB = require('fb');

FB.options(config.socialNetworks.facebook.constructorOptions);


/**
 *
 * @type {RedisClient}
 */
const redis = redisCl.createClient(config.redis);

/** @type {Pool} */
const mysql = Mysql.createPool(config.mysql);

/**
 *
 * @type {Client}
 */
const es = new ES.Client(config.elasticsearch);

module.exports = {
    mysql,
    config,
    es,
    redis,
    logger: proxy,
    FB
};