"use strict";

class WeakContainerMap {
    constructor() {
        this._map = new Map();
    }

    add(k, v){
        let container = this._map.get(k);

        if (container) {
            container.add(v);
        }

        else {
            this._map.set(k, new WeakSet([v]));
        }
    }

    delete(k) {
        let ret = this._map.get(k);

        if (ret) {
            this._map.delete(k);
            return ret.values();
        }

    }
}

module.exports = WeakContainerMap;