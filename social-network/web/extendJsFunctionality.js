"use strict";

String.prototype.replaceAt = function (index, replacement, offset) {
    if (offset == null)
        offset = replacement.length;
    return this.substr(0, index) + replacement + this.substr(index + offset);
};