"use strict";

const {MultiLevelAsyncCache} = require("./MultiLevelAsyncCache");
const {mysql, es} = require("./data");
const {User} = require("./User");

const AccountInformationCache = new MultiLevelAsyncCache({
    max: 5000,
    maxAge: 60 * 1000,
    load: function (id, callback) {
        var additionalInfo = this.additionalArgs;

        if (additionalInfo) {
            this.additionalArgs = undefined;
            construct(null, [additionalInfo]);
        }
        else {
            // language=MySQL
            let query = "SELECT * FROM user WHERE id = ?;";

            mysql.query(query, [id], construct);
        }


        function construct(err, rows) {
            if (err || !rows.length)
                return callback(err);

            let userInfo = rows[0];

            es.get({
                type: "_doc",
                index: "person",
                id: userInfo.id
            }, (err, result) => {
                if (err || !result.found)
                    return callback(err);

                callback(null, new User(userInfo, result._source));
            });
        }
    }
});

const CLEANUP = new (require("./WeakContainerMap"))();

const KEEP_ALIVE = Symbol('keep alive');

const AccountInformationCacheByFid = new MultiLevelAsyncCache({
    max: 5000,
    maxAge: 60 * 1000,
    load: function (type, id, callback) {
        let matcher = type + "_id";

        // language=MySQL
        let query = "SELECT * FROM user WHERE ?? = ?;";

        mysql.query(query, [matcher, id], (err, rows) => {
            if (err || !rows.length)
                return callback(err);

            let userInfo = rows[0];

            AccountInformationCache.additionalArgs = userInfo;
            AccountInformationCache.get(userInfo.id, (err, result) => {
                if (result) {
                    var args = [type, id];
                    Object.defineProperty(result, KEEP_ALIVE, {
                        enumerable: false,
                        writable: true,
                        configurable: true,
                        value: args
                    });

                    CLEANUP.add(userInfo.id, args);
                }

                callback(err, result);
            });

        });
    }
});


const PersonInformationCacheByFid = new MultiLevelAsyncCache({
    max: 5000,
    maxAge: 60 * 1000,
    load: function (type, id, callback) {
        let term = {};
        term["foreignIds." + type] = id;

        es.search({
            index: "person",
            type: "_doc",
            body: {
                "query": {
                    "bool": {
                        "filter":
                            {
                                "term": term
                            }
                    }
                }
            }
        }, (err, result) => {
            if (err)
                return callback(err);

            let entry = result.hits.hits[0];

            if (!entry)
                return callback();

            entry._source.id = entry._id;

            callback(null, entry._source);
        });
    }
});

function resetUser(id) {
    AccountInformationCache.del(id);

    let cleanup = CLEANUP.delete(id);
    if (cleanup)
        for (let args of cleanup) {
            AccountInformationCacheByFid.del(...args);
        }
}

module.exports.AccountInformationCache = AccountInformationCache;
module.exports.AccountInformationCacheByFid = AccountInformationCacheByFid;
module.exports.PersonInformationCacheByFid = PersonInformationCacheByFid;
module.exports.resetUser = resetUser;