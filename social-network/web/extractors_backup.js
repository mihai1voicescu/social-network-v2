"use strict";

const {mysql, es} = require("./data");
const WebErrors = require("./WebErrors");


module.exports = {
    /**
     *
     * @param {string|string[]} personIds
     * @param {function(err, Object<string, PostData>)} callback
     */
    getPersons: function (personIds, callback) {
        if (Array.isArray(personIds)) {
            es.search({
                index: "person",
                type: "_doc",
                body: {
                    "query": {
                        "ids": {
                            "values": personIds
                        }
                    }
                }
            }, (err, result) => {
                let ret = {};

                if (err)
                    return callback(err, ret);


                for (let hit of result.hits.hits) {
                    let id = hit.id;

                    let source = hit._source;

                    source.id = id;

                    ret[id] = source;
                }
                callback(null, ret);
            });
        }
        else {
            es.get({
                index: "person",
                type: "_doc",
                id: personIds
            }, (err, result) => {
                let ret = {};

                if (err || !result.found)
                    return callback(err, ret);


                let id = result.id;

                let source = result._source;

                source.id = id;

                ret[id] = source;

                callback(null, ret);
            });
        }
    },

    /**
     *
     * @param userId
     * @param callback
     * @param from
     * @param count
     * @param replySize
     * @param {function(err, PostData)} callback
     */
    getPosts: function (userId, callback, from = 0, count = 20, replySize = 10, subReplySize = 3) {
        es.search({
                index: "post",
                type: "_doc",
                size: 0,
                body:
                    {
                        "query": {
                            "term": {
                                "userId": {
                                    "value": userId
                                }
                            }
                        },
                        "aggs": {
                            "threads": {
                                "terms": {
                                    "field": "thread"
                                },
                                "aggs": {
                                    "max_date": {
                                        "max": {
                                            "field": "createdDate"
                                        }
                                    },
                                    "sales_bucket_sort": {
                                        "bucket_sort": {
                                            "sort": [
                                                {
                                                    "max_date": {
                                                        "order": "desc"
                                                    }
                                                }
                                            ],
                                            "size": count,
                                            "from": from
                                        }
                                    },
                                    "getParent": {
                                        "filter": {
                                            "term": {
                                                "type": "post"
                                            }
                                        },
                                        "aggs": {
                                            "hits": {
                                                "top_hits": {
                                                    "size": count
                                                }
                                            }
                                        }
                                    },
                                    "getReply": {
                                        "children": {
                                            "type": "reply"
                                        },
                                        "aggs": {
                                            "hits": {
                                                "top_hits": {
                                                    "size": replySize
                                                }
                                            },
                                            "getSubReply": {
                                                "children": {
                                                    "type": "reply"
                                                },
                                                "aggs": {
                                                    "hits": {
                                                        "top_hits": {
                                                            "size": subReplySize
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
            },
            /**
             *
             * @param err
             * @param {SearchResponse} result
             */
            function (err, result) {
                if (err)
                    return callback(err);

                let posts = [];

                for (let container of result.hits.hits) {
                    let hits = container.inner_hits.result.hits.hits;
                    let source = hits[0]._source;

                    let children = [];
                    let post = {
                        id: hits[0]._id,
                        children: children,
                        total: container.inner_hits.result.hits.total
                    };

                    Object.assign(post, source);
                    for (let i = 1, len = hits.length; i < len; i++) {
                        source = hits[i]._source;
                        source.id = hits[i]._id;

                        children.push(source);
                    }

                    posts.push(post);
                }

                callback(null, posts);

            });
    },

    getUserInfoByCookie: function (cookie, callback) {
        mysql.query(/* language=MySQL */ "SELECT user.* FROM cookie JOIN user ON (user_id=user.id)   WHERE cookie=?;", [cookie], (err, rows) => {
            if (err)
                return callback(err);

            if (!rows.length)
                return callback(new WebErrors.InvalidCredentials());

            callback(null, rows[0]);
        });
    }
}
;


/**
 *
 * @class ReplyData
 *
 * @property {string} id
 * @property {string} userId
 * @property {string} personId
 * @property {string} createdDate
 * @property {{}} foreignIds
 * @property {string} network
 * @property {string[]} photos
 * @property {string} content
 * @property {string} type
 * @property {string} thread
 */


/**
 *
 * @class PostData
 * @extends ReplyData
 *
 * @property {ReplyData[]} children
 */


/**
 *
 * @class PersonData
 *
 * @property {string} id
 * @property {string} createdDate
 * @property {string} type
 * @property {string} firstName
 * @property {string} lastName
 * @property {string} email
 * @property {string} avatar
 * @property {number} positivity
 * @property {{}} foreignIds
 */
