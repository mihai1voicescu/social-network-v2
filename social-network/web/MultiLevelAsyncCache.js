"use strict";

const AsyncCache = require('async-cache');

class MultiLevelAsyncCache extends AsyncCache {
    /**
     *
     * @param {{max, maxAge, load}} opt
     */
    constructor(opt) {
        let actualLoad = opt.load;

        opt.load = (key, callback) => {
            let args = key.split('\x07');

            args.push(callback);

            actualLoad.apply(this, args);
        };
        super(...arguments);

        this.additionalArgs = undefined;
    }

    get(...args) {
        let callback = args.pop();
        let key = args.join('\x07');

        super.get(key, callback);
    }

    del(... args) {
        let key = args.join('\x07');
        return super.del(key);
    }
}

module.exports = {MultiLevelAsyncCache};
