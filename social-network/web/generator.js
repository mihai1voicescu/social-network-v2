"use strict";

var crypto = require("crypto");

module.exports = {
    generateEsId: function () {
        return crypto.randomBytes(28).toString('base64').replace(/\+/g, '-').replace(/\//g, '_');
    }
};
