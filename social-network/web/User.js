"use strict";

const socialNetworks = require("./social-networks");

const {logger, es} = require("./data");

const cluster = require("./cluster");
const WebErrors = require("./WebErrors");
const {generateEsId} = require("./generator");

const vader = require('vader-sentiment');

function calculatePositivity(text) {
    let sentences = text.split(/[.?!:;]+/g);
    let score = 0, counts = 0;
    for (let sentence of sentences) {
        if (sentence.length > 1) {
            score += vader.SentimentIntensityAnalyzer.polarity_scores(sentence).compound;
            counts++;
        }

    }

    return score / counts;
}

class User {
    constructor(userInfo, personInfo) {
        /**
         *
         * @type {Map<string, SocialNetwork>}
         */
        this.addapters = new Map();
        this.userInfo = userInfo;
        this.personInfo = personInfo;
        this.id = userInfo.id;

        for (let Cons of Object.values(socialNetworks)) {
            if (Cons.checkIntegration(userInfo)) {
                /**
                 *
                 * @type {SocialNetwork}
                 */
                let adapter = new Cons(userInfo, this);
                this.addapters.set(adapter.type, adapter);
            }


        }
    }

    /**
     *
     * @param type
     * @return {SocialNetwork | undefined}
     */
    getAdapter(type) {
        return this.addapters.get(type);
    }

    /**
     *
     * @param {function(SocialNetwork): Promise} eachFunction
     * @param {function(err, {}[])} finish
     * @param callback
     */
    runThroughAdapters(eachFunction, finish, callback) {
        let promises = [];

        if (this.addapters) {
            this.addapters.forEach(eachFunction, promises);
        }

        if (promises.length)
            Promise.all(promises).then(finish, (err) => {
                logger.error(err, "Could not finish storing in social networks");

                callback(new WebErrors.ThirdPartyError("Could not save "));
            });
        else finish();
    }

    /**
     *
     * @param content
     * @param raw
     * @param photos
     * @param callback
     */
    postGlobal(content, raw, photos, callback) {
        let createdDate = Date.now();

        const finish = (responses) => {

            let foreignIds = {};

            if (responses) {
                Object.assign(foreignIds, ...responses);
            }

            let id = generateEsId();

            let body = {
                userId: this.id,
                content,
                raw,
                foreignIds,
                createdDate,
                thread: id,
                network: "global",
                threadNetwork: "global",
                positivity: calculatePositivity(content),
                type: "post",
                metrics: {
                    likes: 0
                }
            };

            es.create({
                id,
                index: "post",
                type: "_doc",
                body
            }, (err) => {
                if (err)
                    return callback(err);

                body.id = id;

                cluster.dispatchToUser(this.id, body, "new/post");
                callback();
            });
        };

        this.runThroughAdapters(function (adapter) {
            this.push(adapter.post(content, raw, photos).catch(err => {
                logger.error(err, "Unable to store post in a social network");

                return {};
            }));
        }, finish, callback);

    }

    reply(id, routing, content, raw, photos, callback) {
        es.get({
                index: "post",
                type: "_doc",
                routing,
                id
            },
            /**
             *
             * @param err
             * @param {GetResponse} result
             */
            (err, result) => {
                if (err)
                    return callback(err);

                /**
                 * @type {PostData}
                 */
                let source = result._source;

                if (source.network === 'global') {
                    this.replyGlobal(result, content, raw, photos, callback);
                }
                else {
                    this.replyPost(result, content, raw, photos, callback);
                }

            });
    }

    /**
     *
     * @param {GetResponse} parent
     * @param {string} content
     * @param raw
     * @param {URL[]} photos
     * @param callback
     */
    replyGlobal(parent, content, raw, photos, callback) {
        /**
         * @type {PostData}
         */
        let parentSource = parent._source;

        this.runThroughAdapters(function (adapter) {
            this.push(adapter.reply(parentSource.foreignIds, content, raw, photos));
        }, (responses) => {
            let foreignIds = {};

            if (responses) {
                Object.assign(foreignIds, ...responses);
            }

            this._storeReply(parentSource, content, raw, foreignIds, parent._id, callback);
        }, callback);
    }

    _storeReply(parentSource, content, raw, foreignIds, parentId, callback, network, personId, createdDate = Date.now()) {
        let id = generateEsId();
        let type;
        if (typeof parentSource.type === "string") {
            type = parentSource.type === "post" ? "reply" : "subReply";
        }
        else {
            type = parentSource.type.name === "post" ? "reply" : "subReply";
        }

        let body = {
            userId: this.id,
            content,
            raw,
            personId,
            foreignIds,
            createdDate,
            thread: parentSource.thread,
            network: network || parentSource.network,
            threadNetwork: parentSource.threadNetwork,
            positivity: calculatePositivity(content),
            type: {
                name: type,
                parent: parentId
            },
            metrics: {
                likes: 0
            }
        };

        es.create({
            id,
            routing: parentSource.thread,
            index: "post",
            type: "_doc",
            body
        }, (err) => {
            if (err)
                return callback(err);

            body.id = id;

            cluster.dispatchToUser(this.id, body, "new/reply");
            callback();
        });

    }


    /**
     *
     * @param {GetResponse} parent
     * @param {string} content
     * @param raw
     * @param {URL[]} photos
     * @param callback
     */
    replyPost(parent, content, raw, photos, callback) {
        /**
         * @type {PostData}
         */
        let parentSource = parent._source;

        let adapter = this.getAdapter(parentSource.network);

        adapter.reply(parentSource.foreignIds, content, raw, photos).then((foreignIds) => {
            this._storeReply(parentSource, content, raw, foreignIds, parent._id, callback);
        }, callback);
    }

    sendMessage(content, callback) {

    }

    /**
     *
     * @param content
     * @param {{}} foreignIds
     * @param createdDate
     * @param network
     * @param photos
     * @param callback
     * @param personId
     */
    handlePost(content, foreignIds, createdDate, network, photos, callback, personId) {

        let id = generateEsId();

        let body = {
            userId: this.id,
            personId,
            content,
            foreignIds,
            createdDate,
            thread: id,
            network,
            threadNetwork: network,
            positivity: calculatePositivity(content),
            type: "post",
            metrics: {
                likes: 0
            }
        };

        es.create({
            id,
            index: "post",
            type: "_doc",
            body
        }, (err) => {
            if (err)
                return callback(err);

            body.id = id;

            cluster.dispatchToUser(this.id, body, "new/post");
            callback();
        });
    }

    storePerson(firstName, lastName, email, foreignIds, avatar, metrics, callback) {
        if (callback == null) {
            callback = metrics;
            metrics = undefined;
        }
        metrics = metrics || {};
        metrics.likes = metrics.likes || 0;
        let id = generateEsId();

        let body = {
            userId: [this.id],
            "type": "person",
            firstName,
            "suggest": [firstName + " " + lastName, lastName + " " + firstName],
            "lastName": lastName,
            "createdDate": Date.now(),
            email,
            avatar,
            positivity: 0,
            foreignIds,
            metrics
        };

        es.create({
            id,
            index: "person",
            type: "_doc",
            body
        }, (err) => {
            if (err)
                return callback(err);

            body.id = id;


            callback(null, body);
        });

    }

    sharePerson(id, callback) {
        es.update({
            id,
            index: "person",
            "type": "_doc",
            source: true,
            body: {
                "script": {
                    "source": "ctx._source.userId.add(params.uId)",
                    "lang": "painless",
                    "params": {
                        "uId": this.id
                    }
                }
            }
        }, (err, result) => {
            if (err)
                return callback(err);

            if (result.get.found) {

                result = result.get._source;
                result.id = id;
                callback(null, result);
            }
            else callback();
        });
    }

    likePost(id, type, callback) {
        var script = {
            "source": "if (ctx._source.metrics == null) {ctx._source.metrics=['likes': 1]} else if (ctx._source.metrics.likes == null) {ctx._source.metrics.likes = 1} else {ctx._source.metrics.likes++} ",
            "lang": "painless"
        };
        if (type) {
            var term = {};
            term["foreignIds." + type] = id;
            es.updateByQuery({
                index: "post",
                type: "_doc",
                body: {
                    "query": {
                        term
                    },
                    script
                }
            },callback);
        }
        else {
            es.update({
                index: "post",
                type: "_doc",
                id,
                body: {
                    script
                }
            }, callback);
        }
    }

    handleReply(network, parentId, content, fid, photos, createdDate, personId, callback) {
        let fidTerms = {};

        fidTerms["foreignIds." + network] = parentId;
        es.search({
                index: "post",
                type: "_doc",
                body: {
                    "query": {
                        "bool": {
                            "filter": [{
                                "term": {
                                    "userId": this.id
                                }
                            },
                                {
                                    "term": fidTerms
                                }]
                        }
                    }
                }
            },

            (err, result) => {
                if (err)
                    return callback(err);

                let parent = result.hits.hits[0];

                if (!parent) {
                    logger.error("Could not find parent.");

                    return callback();
                }

                /**
                 * @type {PostData}
                 */
                let parentSource = parent._source;

                this._storeReply(parentSource, content, undefined, fid, parent._id, callback, network, personId, createdDate);
            });

    }

    handleMessage(content, callback) {

    }
}

module.exports = {User};