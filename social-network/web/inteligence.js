"use strict";

const schedule = require('node-schedule');

const {getReach} = require("./extractors");
const {mysql, logger, es} = require("./data");

schedule.scheduleJob('0 * * * *', sync);


function sync() {

    //language=MySQL
    var query = "SELECT id FROM user;";

    mysql.query(query, (err, rows) => {
        if (err)
            return logger.error(err, "Unable to select users.");


        rows.forEach(row => {
            let userId = row.id;

            getReach(userId, (err, reach) => {
                if (err)
                    return logger.error(err, "Unable to calculate reach.");


                es.index({
                    index: "timeseries",
                    type: "_doc",
                    body: {
                        createdDate: Date.now(),
                        reach,
                        userId
                    }
                }, (err) => {
                    if (err)
                        logger.error("Unable to save timeseries data for user " + userId);
                });
            });
        });
    });
}