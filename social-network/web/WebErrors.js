"use strict";

class WebError extends Error {
    constructor(statusCode, code, message, cause) {
        super(message);
        this.statusCode = statusCode;
        this.code = code || 500;
        this.cause = cause;
        Error.captureStackTrace(this, WebError);
        if (cause)
            this.stack += "\nCausedBy:" + cause.stack;
    }

    toResponse(res) {
        res.status(this.statusCode);
        res.send({
            code: this.code,
            message: this.message,
            cause: this.cause
        });
    }
}

class AuthError extends WebError {
    constructor(...args) {

        super(...args);
    }
}

class AuthentificationError extends AuthError {
    constructor(message, cause) {
        super(403, "AUTHORIZATION_ERROR", message, cause);
    }
}

class InvalidCredentials extends AuthError {
    constructor(cause) {
        super(403, "AUTHORIZATION_ERROR", "Wrong username or password.", cause);
    }
}

class InternalServerError extends WebError {
    constructor(cause) {
        super(500, "INTERNAL_SERVER_ERROR", "This time no news is bad news...", cause);
    }
}


class UnsuportedSelection extends WebError {
    constructor(cause) {
        super(400, "INVALID_SELECTION", "The given parameters are invalid.", cause);
    }
}


class ThirdPartyError extends WebError {
    constructor(message, cause) {
        super(503, "THIRD_PARTY", message || "Third party error.", cause);
    }
}


class NotFound extends WebError {
    constructor(message = "Entity is missing.", cause) {
        super(404, "NOT_FOUND", message, cause);
    }
}

class SubsequentError extends Error {
    constructor(message, cause) {
        super(message);
        this.cause = cause;
        Error.captureStackTrace(this, SubsequentError);
        if (cause)
            this.stack += "\nCausedBy:" + cause.stack;
    }
}


module.exports = {
    WebError,
    AuthentificationError,
    InvalidCredentials,
    InternalServerError,
    AuthError,
    UnsuportedSelection,
    ThirdPartyError,
    NotFound,
    SubsequentError
};