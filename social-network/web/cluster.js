"use strict";

const {redis, logger, config} = require("./data");

const peers = new Map();

/**
 *
 * @type {Map<string, Set<any>>}
 */
const connections = new Map();

function addConnection(socket, userInfo) {
    socket.userInfo = userInfo;

    let userConnections = connections.get(userInfo.id);
    if (!userConnections) {
        userConnections = new Set();
        connections.set(userInfo.id, userConnections);
    }


    let closeHandler = err => {
        if (err)
            logger.error(err, "Socket error.");
        else logger.info("Socket disconnect.");

        connections.delete(socket);
    };
    socket.once('close', closeHandler);
    socket.once('error', closeHandler);

    userConnections.add(socket);
}

const PROC_LIST = "al:proc";
start();

function start() {
    let from = Date.now() - config.cluster.heartBeat;
    redis.zrangebyscore(PROC_LIST, from, Infinity, (err, entries) => {
        if (err)
            throw err;

        console.log(entries);
    });
}

var ID = config.web.host + config.web.port;

function register() {
    redis.zadd(PROC_LIST, Date.now(), ID, err => {
        if (err)
            console.error(err, "Unable to refresh");
    });
}

register();
setInterval(register, config.cluster.heartBeat);

module.exports = {
    addConnection: addConnection,
    dispatchToUser: function (user, payload, event) {

        var conn = connections.get(user);
        if (conn)
            conn.forEach(function (socket) {
                socket.emit(event, {payload});
            });
        else logger.info("No active connections to user " + user);

    }
};
