"use strict";

class NotImplemented extends Error {

}

/**
 * @property {string} type
 *
 * @interface
 */

class SocialNetwork {

    /**
     *
     * @constructs
     * @param userInfo
     * @param {User} user
     * @param [callback]
     */
    constructor(userInfo, user, callback) {
        this.user = user;
        if (callback)
            callback();
    }

    /**
     * @abstract
     * @param content
     * @param raw
     * @param photos
     * @return Promise
     */
    post(content, raw, photos) {
    }
    //
    // /**
    //  * @abstract
    //  * @param data
    //  * @param callback
    //  */
    // registerNewPostListener(fn, callback) {
    //     throw new NotImplemented();
    // }


    /**
     *
     * @abstract
     * @param {{}} foreignIds
     * @param content
     * @param raw
     * @return {Promise}
     */
    reply(foreignIds, content, raw, photos) {
    }

    getPersonInfo(id, callback) {
    }
}

SocialNetwork.prototype.type = "SocialNetwork";
SocialNetwork.NEEDED = [];

SocialNetwork.checkIntegration = function (userInfo) {
    for (let field of this.NEEDED) {
        if (userInfo[field] == null)
            return false;
    }

    return true;
};


module.exports = {SocialNetwork};
