"use strict";

const {FacebookConnector} = require("./FacebookConnector");
const {TwitterConnector} = require("./TwitterConnector");

/**
 *
 * @type {Object<string, SocialNetwork>}
 */
module.exports = {
    FacebookConnector,
    TwitterConnector
};