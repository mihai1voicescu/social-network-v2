"use strict";

const {config, redis, logger, FB, mysql} = require("../data");
const {Router} = require("express");
const WebErrors = require("../WebErrors");
const AccountInformationCache = require("../AccountInformationCache");
// ES2015 w/ require()


const {SocialNetwork} = require("./SocialNetwork");

const _1_DAY = 24 * 3600;

function markKey(key, callback) {
    redis.set("fb:p:" + key, Date.now(), "EX", _1_DAY, callback);
}

function markKeyL(key, callback) {
    redis.set("fb:l:" + key, Date.now(), "EX", _1_DAY, callback);
}

class FacebookConnector extends SocialNetwork {

    /**
     * @inheritDoc
     */
    constructor(userInfo) {
        super(...arguments);
        this.token = userInfo.facebook_token;
        this.pageId = userInfo.facebook_page_id;
        this.pageToken = userInfo.facebook_page_token;
    }

    /**
     *
     * @param content
     * @param {RawContent} raw
     * @private
     */
    _convertRaw(content, raw) {
        /**
         *
         * @type {Map<string, PersonData>}
         */
        let needsReplace = new Map();
        for (let key in raw.entityMap) {
            /**
             *
             * @type {{type, data: {mention: PersonData, emojiUnicode: string}}}
             */
            let entity = raw.entityMap[key];

            if (entity.type === "mention" && entity.data.mention.foreignIds.facebook) {
                needsReplace.set(key, entity.data.mention);
            }
        }

        let fbContent;
        if (!needsReplace.size)
            fbContent = content;
        else {
            let blocks = [];
            for (let block of raw.blocks) {
                let text = block.text;
                /**
                 * @type {{offset, length, key}[]}
                 */

                if (block.entityRanges.length) {
                    for (let set of block.entityRanges) {
                        let data = needsReplace.get(set.key + "");
                        if (data) {
                            text = text.replaceAt(set.offset, "@[" + data.foreignIds.facebook + "]", set.length);
                        }
                    }
                }

                blocks.push(text);
            }

            fbContent = blocks.join("\n");
        }

        return fbContent;
    }

    /**
     *
     * @param content
     * @param {RawContent} raw
     * @param photos
     * @return {Promise}
     */
    post(content, raw, photos) {

        let fbContent = this._convertRaw(content, raw);

        return new Promise((resolve, reject) => {
            FB.setAccessToken(this.pageToken);

            FB.api(
                "/" + this.pageId + "/feed",
                "POST",
                {message: fbContent},
                (res) => {
                    if (!res || res.error) {
                        console.log(!res ? 'error occurred' : res.error);
                        return reject();
                    }

                    markKey(res.id, (err) => {
                        if (err)
                            reject(err);
                        else
                            resolve({facebook: res.id});
                    });
                }
            );
        });
    }

    /**
     *
     * @param {{}} foreignIds
     * @param content
     * @param raw
     * @param photos
     * @return {Promise}
     */
    reply(foreignIds, content, raw, photos) {
        let id = foreignIds.facebook;

        let fbContent = this._convertRaw(content, raw);


        return new Promise((resolve, reject) => {
            FB.setAccessToken(this.pageToken);

            FB.api(
                "/" + id + "/comments",
                "POST",
                {message: fbContent},
                (res) => {
                    if (!res || res.error) {
                        let err = !res || res.error;
                        logger.error(err, "Could not save in facebook.");
                        return reject(err);
                    }

                    markKey(res.id, (err) => {
                        if (err)
                            reject(err);
                        else
                            resolve({facebook: res.id});
                    });
                }
            );
        });
    }

    getPersonInfo(id, callback) {
        FB.setAccessToken(this.pageToken);

        FB.api(
            '/' + id,
            'GET',
            {},
            (res) => {
                if (!res || res.error) {
                    let err = !res || res.error;
                    logger.error(err, "Could not save in facebook.");
                    return callback(err);
                }

                let {first_name, last_name, email, name} = res;

                if (!first_name && !last_name) {
                    if (name) {
                        let splitIndex = name.lastIndexOf(" ");

                        first_name = name.slice(0, splitIndex);
                        last_name = name.slice(splitIndex);
                    }
                }

                FB.setAccessToken(this.token);

                FB.api(
                    '/' + id + '/friends',
                    'GET',
                    {},
                    (res) => {
                        let followers;
                        if (!res || res.error) {
                            let err = !res || res.error;
                            logger.error(err, "Could not save in facebook.");
                        }
                        else {
                            try {
                                followers = res.summary.total_count;
                            } catch (e) {
                            }
                        }

                        this.user.storePerson(first_name, last_name, email, {facebook: id}, this._calculateAvatar(id), {followers}, callback);

                    });

            }
        );
    }

    _calculateAvatar(facebookId) {
        return "http://graph.facebook.com/" + facebookId + "/picture?type=square";
    }
}

FacebookConnector.prototype.type = FacebookConnector.type = "facebook";

FacebookConnector.NEEDED = ['facebook_token', 'facebook_page_id', 'facebook_page_token'];

const router = new Router();
const xhub = require('express-x-hub');

router.use(xhub({
    algorithm: 'sha1',
    secret: config.socialNetworks.facebook.webhook.secret
}));

router.post("/", function (req, res, next) {
    var promisses = [];
    logger.trace(req.body, "Received webhook.");
    /**
     * @typedef {{
     *      from: {id: string, name: string},
     *      item: string,
     *      comment_id: string,
     *      post_id: string,
     *      verb: string,
     *      parent_id: string,
     *      created_time: number,
     *      post: {
     *          type: string,
     *          updated_time: string,
     *          id: string,
     *          status_type: string,
     *          is_published: true
     *      }
     *      message: string
     *  }} ValueData
     *
     * @type {{changes:{
     *  field: string,
     *  value: ValueData
     * }[], id:string, time:number}[]}
     */
    let entry = req.body.entry;
    switch (req.body.object) {
        case "page":
            entry.forEach(entry => {
                let id = entry.id;
                entry.changes.forEach(change => {
                    switch (change.field) {
                        case "feed":
                            promisses.push(feedHandler(id, change.value));
                            break;

                        default:
                            logger.error(entry, "Unhandled field " + change.field + " from Facebook.");
                    }
                });
            });

            break;

        default:
            logger.error(req.body, "Unhandled webhook from Facebook.");
    }


    if (promisses.length)
        Promise.all(promisses).then(() => {
            res.sendStatus(200);
        }, (err) => {
            next(err);
        });
    else res.sendStatus(200);
});

/**
 *
 * @param id
 * @param {ValueData} valueData
 * @return {Promise}
 */
function feedHandler(id, valueData) {
    return new Promise((resolve, reject) => {

        if (valueData.item === 'reaction') {
            var postId = (valueData.comment_id || valueData.post_id);
            redis.get("fb:l:" + postId, (err, result) => {
                if (err)
                    reject(err);
                else {
                    if (!result) {
                        // actually process
                        AccountInformationCache.AccountInformationCacheByFid.get('facebook_page', id,
                            /**
                             *
                             * @param err
                             * @param {User} user
                             */
                            (err, user) => {
                                if (err || !user)
                                    return reject(err || new WebErrors.NotFound());

                                // todo mark with comment_id
                                markKeyL(postId, (err) => {
                                    if (err)
                                        reject(err);

                                    user.likePost(postId, 'facebook', (err) => {
                                        if (err)
                                            reject(err);
                                        else resolve();
                                    });
                                });
                            });
                    }
                }
            });
            return;
        }
        redis.get("fb:p:" + (valueData.comment_id || valueData.post_id), (err, result) => {
            if (err)
                reject(err);
            else {
                if (!result) {
                    // actually process
                    AccountInformationCache.AccountInformationCacheByFid.get('facebook_page', id,
                        /**
                         *
                         * @param err
                         * @param {User} user
                         */
                        (err, user) => {
                            if (err || !user)
                                return reject(err || new WebErrors.NotFound());

                            // todo mark with comment_id
                            markKey(valueData.post_id, (err) => {
                                if (err)
                                    reject(err);

                                let personId = valueData.from.id;
                                AccountInformationCache.PersonInformationCacheByFid.get("facebook", personId,
                                    /**
                                     *
                                     * @param err
                                     * @param {PersonData} person
                                     * @return {*}
                                     */
                                    (err, person) => {
                                        if (err)
                                            return reject(err);

                                        if (!person) {
                                            user.getAdapter('facebook').getPersonInfo(personId, finish);
                                        }
                                        else if (person) {
                                            if (assureEsField(person.userId, user.id)) {
                                                finish(null, person);
                                            }
                                            else {
                                                user.sharePerson(person.userId, finish);
                                            }
                                        }
                                    });

                            });

                            function finish(err, person) {
                                if (err)
                                    return reject(err);

                                if (valueData.comment_id) {
                                    user.handleReply('facebook', valueData.parent_id, valueData.message, {
                                        facebook: valueData.comment_id
                                    }, undefined, valueData.created_time * 1000, person.id, (err) => {
                                        if (err)
                                            reject(err);
                                        else resolve();
                                    });

                                }
                                else {
                                    user.handlePost(valueData.message, {
                                            facebook: valueData.post_id
                                        }, valueData.created_time * 1000, FacebookConnector.type, undefined,

                                        (err) => {
                                            if (err)
                                                reject(err);
                                            else resolve();
                                        }, person.id);
                                }
                            }

                        });
                }
                else {
                    logger.debug("Message already processed.");
                    resolve();
                }
            }
        });

    });
}

router.get("/", function (req, res) {
    console.log("CALLED WEBHOOK");

    if (
        req.param('hub.mode') === 'subscribe' &&
        req.param('hub.verify_token') === config.socialNetworks.facebook.webhook.verifyToken
    ) {
        res.send(req.param('hub.challenge'));
    } else {
        res.sendStatus(400);
    }
});

FacebookConnector.newUser = function (userId, data, callback) {
    FB.setAccessToken(data.token);

    FB.api('/me/accounts', function (response) {
        var page = response.data[0];

        mysql.query(/* language=MySQL */ "UPDATE user SET facebook_token = ?, facebook_page_token = ?, facebook_page_id = ? WHERE id = ?;", [data.token, page.access_token, page.id, userId], (err, result) => {
            if (err)
                callback(err);

            else if (result.affectedRows !== 1) {
                callback(new WebErrors.InternalServerError());
            }

            else {
                callback();
            }
        });
    });
};

FacebookConnector.webhooksRouter = router;

module.exports = {FacebookConnector};
