"use strict";

const {config, redis, logger, mysql} = require("../data");
const {Router} = require("express");
const WebErrors = require("../WebErrors");
const AccountInformationCache = require("../AccountInformationCache");
const crypto = require("crypto");
// ES2015 w/ require()
const request = require('request')

const twitterRootDomain = "https://api.twitter.com/1.1";

const Twitter = require('twitter');


const {SocialNetwork} = require("./SocialNetwork");

const _1_DAY = 24 * 3600;

function markKey(key, callback) {
    redis.set("tw:p:" + key, Date.now(), "EX", _1_DAY, callback);
}

function markKeyL(key, callback) {
    redis.set("tw:l:" + key, Date.now(), "EX", _1_DAY, callback);
}

class TwitterConnector extends SocialNetwork {

    /**
     * @inheritDoc
     */
    constructor(userInfo) {
        super(...arguments);
        this.token = userInfo.twitter_token;
        this.secret = userInfo.twitter_secret;

        this.twitterId = userInfo.twitter_id;

        this.oauth = {
            consumer_key: config.socialNetworks.twitter.consumerKey,
            consumer_secret: config.socialNetworks.twitter.consumerSecret,
            access_token_key: this.token,
            access_token_secret: this.secret
        };

        /**
         * @type Twitter
         */
        this.client = new Twitter({
            consumer_key: config.socialNetworks.twitter.consumerKey,
            consumer_secret: config.socialNetworks.twitter.consumerSecret,
            access_token_key: this.token,
            access_token_secret: this.secret
        });


        var WEBHOOK_URL = 'https://837cc3ab.ngrok.io/webhook/twitter';

        var request_options = {
            url: 'https://api.twitter.com/1.1/account_activity/all/dev/webhooks.json',
            oauth: {
                consumer_key: config.socialNetworks.twitter.consumerKey,
                consumer_secret: config.socialNetworks.twitter.consumerSecret,
                token: this.token,
                token_secret: this.secret
            }
        };

        // request.get(request_options, function (error, response, body) {
        //     try {
        //         body = JSON.parse(body);
        //     } catch (e) {
        //         logger.error(e, "Error on request");
        //         return;
        //     }
        //     console.log(body);
        //
        //     if (!body.length || !body[0].valid) {
        //
        //         Object.assign(request_options, {
        //             headers: {
        //                 'Content-type': 'application/x-www-form-urlencoded'
        //             },
        //             form: {
        //                 url: WEBHOOK_URL
        //             }
        //         });
        //
        //         request.post(request_options, function (error, response, body) {
        //             console.log(body);
        //         });
        //     }
        //
        // });

    }

    /**
     *
     * @param content
     * @param {RawContent} raw
     * @private
     */
    _convertRaw(content, raw) {
        /**
         *
         * @type {Map<string, PersonData>}
         */
        // let needsReplace = new Map();
        // for (let key in raw.entityMap) {
        //     /**
        //      *
        //      * @type {{type, data: {mention: PersonData, emojiUnicode: string}}}
        //      */
        //     let entity = raw.entityMap[key];
        //
        //     if (entity.type === "mention" && entity.data.mention.foreignIds.facebook) {
        //         needsReplace.set(key, entity.data.mention);
        //     }
        // }
        //
        // let fbContent;
        // if (!needsReplace.size)
        //     fbContent = content;
        // else {
        //     let blocks = [];
        //     for (let block of raw.blocks) {
        //         let text = block.text;
        //         /**
        //          * @type {{offset, length, key}[]}
        //          */
        //
        //         if (block.entityRanges.length) {
        //             for (let set of block.entityRanges) {
        //                 let data = needsReplace.get(set.key + "");
        //                 if (data) {
        //                     text = text.replaceAt(set.offset, "@[" + data.foreignIds.facebook + "]", set.length);
        //                 }
        //             }
        //         }
        //
        //         blocks.push(text);
        //     }
        //
        //     fbContent = blocks.join("\n");
        // }

        return content;
    }

    /**
     *
     * @param content
     * @param {RawContent} raw
     * @param photos
     * @return {Promise}
     */
    post(content, raw, photos) {

        let status = this._convertRaw(content, raw);

        return new Promise((resolve, reject) => {

            this.client.post('/statuses/update', {status}, (err, result) => {
                if (err)
                    reject(err);


                let id = result.id_str;
                let twitterScreenName = result.user.screen_name;
                markKey(id, (err) => {
                    if (err)
                        reject(err);
                    else resolve({twitter: id, twitterScreenName});
                });
            });
        });
    }

    /**
     *
     * @param {{}} foreignIds
     * @param content
     * @param raw
     * @param photos
     * @return {Promise}
     */
    reply(foreignIds, content, raw, photos) {
        let id = foreignIds.twitter;
        let screenName = foreignIds.twitterScreenName;

        let status = this._convertRaw(content, raw);

        if (status.indexOf('@' + screenName) === -1) {
            status = '@' + screenName + ' ' + status;
        }


        return new Promise((resolve, reject) => {

            this.client.post('/statuses/update', {status, in_reply_to_status_id: id}, (err, result) => {
                if (err)
                    reject(err);


                let id = result.id_str;
                let twitterScreenName = result.user.screen_name;
                markKey(id, (err) => {
                    if (err)
                        reject(err);
                    else resolve({twitter: id, twitterScreenName});
                });
            });
        });
    }

}

TwitterConnector.prototype.type = TwitterConnector.type = "twitter";

TwitterConnector.NEEDED = ['twitter_token', 'twitter_secret'];

const router = new Router();
const xhub = require('express-x-hub');

router.use(xhub({
    algorithm: 'sha1',
    secret: config.socialNetworks.facebook.webhook.secret
}));

router.post("/", function (req, res, next) {
    logger.trace(req.body, "Received webhook.");

    /**
     * @type {{
     * for_user_id,
     * favorite_events: Array<{
     *  id,
     *  timestamp_ms,
     *  favorited_status: {
     *      id_str
     * }}>,
     * tweet_create_events: Array<{
     *  id_str: string,
     *  text: string,
     *  in_reply_to_status_id_str: string?,
     *  in_reply_to_user_id_str: string?,
     *  user: {
     *  id_str
     *  }
     * }>,
     * }}
     */
    let body = req.body;


    AccountInformationCache.AccountInformationCacheByFid.get('twitter', body.for_user_id,
        /**
         *
         * @param err
         * @param {User} user
         */
        (err, user) => {
            if (err)
                return next(err);

            if (!user)
                return next(WebErrors.NotFound("User not found."));

            let promisses = [];

            if (body.tweet_create_events) {
                body.tweet_create_events.forEach((createEvent) => {
                    promisses.push(new Promise((resolve, reject) => {

                        redis.get("tw:p:" + createEvent.id_str, (err, result) => {
                            if (err)
                                reject(err);
                            else {
                                if (!result) {
                                    markKey(createEvent.id_str, (err) => {
                                        if (err)
                                            return reject(err);


                                        AccountInformationCache.PersonInformationCacheByFid.get("twitter", createEvent.user.id_str,
                                            /**
                                             *
                                             * @param err
                                             * @param {PersonData} person
                                             * @return {*}
                                             */
                                            (err, person) => {
                                                if (err)
                                                    return reject(err);


                                                if (!person) {
                                                    let names = createEvent.user.name.split(' ');
                                                    let lastName = names.pop();
                                                    let firstName = names.join(' ');
                                                    user.storePerson(firstName, lastName, undefined, {
                                                        twitter: createEvent.user.id_str,
                                                        twitterScreenName: createEvent.user.screen_name
                                                    }, createEvent.user.profile_image_url, {
                                                        followers: createEvent.user.followers_count
                                                    }, finish);
                                                }
                                                else if (person) {
                                                    if (assureEsField(person.userId, user.id)) {
                                                        finish(null, person);
                                                    }
                                                    else {
                                                        user.sharePerson(person.userId, finish);
                                                    }
                                                }

                                                /**
                                                 *
                                                 * @param err
                                                 * @param {PersonData} person
                                                 * @return {*}
                                                 */
                                                function finish(err, person) {
                                                    if (err)
                                                        return reject(err);

                                                    if (createEvent.in_reply_to_status_id_str) {
                                                        user.handleReply('twitter', createEvent.in_reply_to_status_id_str, createEvent.text, {
                                                            twitterScreenName: createEvent.user.screen_name,
                                                            twitter: createEvent.id_str
                                                        }, undefined, +createEvent.timestamp_ms, person.id, (err) => {
                                                            if (err)
                                                                reject(err);
                                                            else resolve();
                                                        });

                                                    }
                                                    else {
                                                        user.handlePost(createEvent.text, {
                                                                twitterScreenName: createEvent.user.screen_name,

                                                                twitter: createEvent.id_str
                                                            }, +createEvent.timestamp_ms, TwitterConnector.type, undefined,

                                                            (err) => {
                                                                if (err)
                                                                    reject(err);
                                                                else resolve();
                                                            }, person.id);
                                                    }
                                                }

                                            });
                                    });
                                }
                            }
                        });
                    }));

                });
            }

            if (body.favorite_events) {
                body.favorite_events.forEach((favoriteEvent) => {
                    promisses.push(new Promise((resolve, reject) => {

                        redis.get("tw:l:" + favoriteEvent.id, (err, result) => {
                            if (err)
                                reject(err);
                            else {
                                if (!result) {
                                    markKeyL(favoriteEvent.id, (err) => {
                                        if (err)
                                            return reject(err);


                                        user.likePost(favoriteEvent.favorited_status.id_str, 'twitter', function (err) {
                                            if (err)
                                                reject(err);
                                            else resolve();
                                        });
                                    });
                                }
                            }
                        });
                    }));

                });
            }

            if (promisses.length)
                Promise.all(promisses).then(() => {
                    res.sendStatus(200);

                }, (err) => {
                    next(err);
                });
            else res.sendStatus(200);
        });

});


router.get("/", function (req, res) {
    const hmac = crypto.createHmac('sha256', config.socialNetworks.twitter.consumerSecret);

    var reqQuery = req.query;
    hmac.update(reqQuery.crc_token);
    var sha256_hash_digest = hmac.digest('base64');

    res.status(200).send({response_token: "sha256=" + sha256_hash_digest});
});

TwitterConnector.newUser = function (userId, data, callback) {
    console.error("TWITTER NEW USER NOT IMPLEMENTED");
    callback();
};

TwitterConnector.webhooksRouter = router;

module.exports = {TwitterConnector};


/**
 * Retieves user ID for access tokens in config
 * and adds it to twitter object
 */
function get_user_id(twitterOauth, callback) {

    let request_options = {
        url: 'https://api.twitter.com/1.1/account_activity/all/prod/webhooks',
        oauth: twitterOauth
    };

    // get current user info
    request.get(request_options, function (error, response, body) {

        if (error) {
            return callback(error);
        }

        body = JSON.parse(body);
        console.log(require('util').inspect(body, {depth: null, colors: true}));

        callback(null, body.id_str);
    });
}

//
// setTimeout(() => {
// }, 1500); // turn on twitter requests
