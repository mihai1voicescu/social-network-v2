"use strict";

const crypto = require("crypto");

module.exports = {
    generateCookie: function () {
        return crypto.randomBytes(32).toString('hex');
    }
};
