"use strict";

const {mysql, es} = require("./data");
const WebErrors = require("./WebErrors");


module.exports = {
    /**
     *
     * @param search
     * @param userId
     * @param {function(err, PersonData[])} callback
     * @param limit
     */
    searchPerson: function (search, userId, callback, limit = 10) {

        if (!search) {
            es.search({
                index: "person",
                type: "_doc",
                size: limit,
                body: {
                    "query": {
                        "bool": {
                            "filter": {
                                "term": {
                                    "userId": userId
                                }
                            }
                        }
                    }
                }
            }, (err, result) => {
                if (err)
                    return callback(err);

                let results = [];

                for (let hit of result.hits.hits) {

                    let source = hit._source;
                    source.id = hit._id;

                    results.push(source);
                }

                callback(null, results);
            });
        }
        else {
            es.search({
                index: "person",
                type: "_doc",
                body: {
                    "suggest": {
                        "name": {
                            "prefix": search,
                            "completion": {
                                "field": "suggest",
                                "size": limit,
                                "contexts": {"userId": userId}
                            }
                        }
                    }
                },
            }, (err, result) => {
                if (err)
                    return callback(err);

                let results = [];
                for (let hit of result.suggest.name[0].options) {
                    let source = hit._source;
                    source.id = hit._id;

                    results.push(source);
                }

                callback(null, results);
            });
        }


    },
    /**
     *
     * @param {string|string[]} personIds
     * @param userId
     * @param {function(err, Object<string, PostData>)} callback
     */
    getPersons: function (personIds, userId, callback) {
        if (Array.isArray(personIds)) {
            es.search({
                index: "person",
                type: "_doc",
                body: {
                    "query": {
                        "bool": {
                            "filter": [
                                {
                                    "term": {
                                        "userId": userId
                                    }
                                },
                                {
                                    "ids": {
                                        "values": personIds
                                    }
                                }
                            ]
                        }
                    }
                }
            }, (err, result) => {
                let ret = {};

                if (err)
                    return callback(err, ret);


                for (let hit of result.hits.hits) {
                    let id = hit.id;

                    let source = hit._source;

                    source.id = id;

                    ret[id] = source;
                }
                callback(null, ret);
            });
        }
        else {
            es.get({
                    index: "person",
                    type: "_doc",
                    id: personIds
                }, (err, result) => {
                    let ret = {};

                    if (err || !result.found)
                        return callback(err, ret);


                    let source = result._source;

                    if (Array.isArray(source.userId)) {
                        if (!source.userId.includes(userId))
                            return callback();
                    }
                    else if (source.userId !== userId)
                        return callback();


                    source.id = personIds;

                    ret[personIds] = source;

                    callback(null, ret);
                }
            );
        }
    },

    /**
     *
     * @param userId
     * @param callback
     * @param from
     * @param count
     * @param replySize
     * @param {function(err, PostData[])} callback
     */
    getPosts: function (userId, callback, from = 0, count = 20, replySize = 10, subReplySize = 3) {
        es.search({
                index: "post",
                type: "_doc",
                size: 0,
                body:
                    {
                        "query": {
                            "term": {
                                "userId": {
                                    "value": userId
                                }
                            }
                        },
                        "aggs": {
                            "threads": {
                                "terms": {
                                    "field": "thread",
                                    "order": {
                                        "max_date": "desc"
                                    },
                                    "size": 2147483647
                                },
                                "aggs": {
                                    "max_date": {
                                        "max": {
                                            "field": "createdDate"
                                        }
                                    },
                                    "sales_bucket_sort": {
                                        "bucket_sort": {
                                            "sort": [
                                                {
                                                    "max_date": {
                                                        "order": "desc"
                                                    }
                                                }
                                            ],
                                            "size": count,
                                            "from": from
                                        }
                                    },
                                    "getParent": {
                                        "filter": {
                                            "term": {
                                                "type": "post"
                                            }
                                        },
                                        "aggs": {
                                            "hits": {
                                                "top_hits": {}
                                            },
                                            "getReply": {
                                                "children": {
                                                    "type": "reply"
                                                },
                                                "aggs": {
                                                    "group": {
                                                        "terms": {
                                                            "field": "_id",
                                                            "order": {
                                                                "getSubReply>max_date": "desc",
                                                                "max_date": "desc"
                                                            },
                                                            "size": replySize
                                                        },
                                                        "aggs": {
                                                            "max_date": {
                                                                "max": {
                                                                    "field": "createdDate"
                                                                }
                                                            },
                                                            "hits": {
                                                                "top_hits": {}
                                                            },
                                                            "getSubReply": {
                                                                "children": {
                                                                    "type": "subReply"
                                                                },
                                                                "aggs": {
                                                                    "max_date": {
                                                                        "max": {
                                                                            "field": "createdDate"
                                                                        }
                                                                    },
                                                                    "hits": {
                                                                        "top_hits": {
                                                                            "size": subReplySize,
                                                                            "sort": [
                                                                                {
                                                                                    "createdDate": "desc"
                                                                                }
                                                                            ]
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
            },
            /**
             *
             * @param err
             * @param {SearchResponse} result
             */
            function (err, result) {
                if (err)
                    return callback(err);

                let posts = [];

                for (let threadBucket of result.aggregations.threads.buckets) {
                    let parent = threadBucket.getParent.hits.hits.hits[0]._source;
                    parent.id = threadBucket.getParent.hits.hits.hits[0]._id;

                    parent.childrenTotal = threadBucket.getParent.getReply.doc_count;

                    if (parent.childrenTotal > 0) {
                        let children = [];
                        for (let replyBucket of threadBucket.getParent.getReply.group.buckets) {
                            let reply = replyBucket.hits.hits.hits[0]._source;
                            reply.id = replyBucket.hits.hits.hits[0]._id;

                            reply.childrenTotal = replyBucket.getSubReply.doc_count;

                            if (reply.childrenTotal > 0) {
                                let children = [];
                                for (let subReplyHit of replyBucket.getSubReply.hits.hits.hits) {
                                    let subReply = subReplyHit._source;
                                    subReply.id = subReplyHit._id;
                                    children.push(subReply);
                                }

                                reply.children = children;
                            }

                            children.push(reply);
                        }
                        parent.children = children;
                    }

                    posts.push(parent);
                }

                callback(null, posts);

            });
    },

    getReach: function (userId, callback) {
        es.search({
            index: 'person',
            type: '_doc',
            size: 400,
            body: {
                "query": {
                    "bool": {
                        "filter": {
                            "term": {
                                "userId": "1"
                            }
                        }
                    }
                }
            }
        }, (err, result) => {
            if (err)
                return callback(err);

            var count = 0;

            for (var hit of result.hits.hits) {
                if (hit._source.metrics && hit._source.metrics.followers)
                    count += hit._source.metrics.followers;
            }

            callback(null, count);
        });
    },


    getTimeseries: function (start, end = Date.now(), userId, callback) {
        es.search({
            index: 'timeseries',
            type: '_doc',
            size: 400,
            body: {
                "sort": [
                    {
                        "createdDate": {
                            "order": "desc"
                        }
                    }
                ],
                "query": {
                    "bool": {
                        "filter": [{
                            "range": {
                                "createdDate": {
                                    "gte": start,
                                    "lte": end
                                }
                            }
                        }, {
                            "term": {
                                "userId": userId
                            }
                        }]
                    }
                }
            }
        }, (err, result) => {
            if (err)
                return callback(err);

            callback(null, result.hits.hits.map(hit => hit._source));
        });
    },

    getUserInfoByCookie: function (cookie, callback) {
        mysql.query(/* language=MySQL */ "SELECT user.* FROM cookie JOIN user ON (user_id=user.id)   WHERE cookie=?;", [cookie], (err, rows) => {
            if (err)
                return callback(err);

            if (!rows.length)
                return callback(new WebErrors.InvalidCredentials());

            callback(null, rows[0]);
        });
    }
}
;


/**
 *
 * @class ReplyData
 *
 * @property {string} id
 * @property {string} userId
 * @property {string} personId
 * @property {number} createdDate
 * @property {{}} foreignIds
 * @property {string} network
 * @property {string[]} photos
 * @property {string} content
 * @property {string|{name, parent}} type
 * @property {string} thread
 */


/**
 *
 * @class PostData
 * @extends ReplyData
 *
 * @property {ReplyData[]} children
 */


/**
 *
 * @class PersonData
 *
 * @property {string} id
 * @property {string} userId
 * @property {string} createdDate
 * @property {string} type
 * @property {string} firstName
 * @property {string} lastName
 * @property {string} email
 * @property {string} avatar
 * @property {number} positivity
 * @property {{}} foreignIds
 * @property {{}} metrics
 */


/**
 *
 * @typedef {{text:string, entityRanges: {offset, length, key}[] }} Block
 */
/**
 * @class RawContent
 *
 * @property {Block[]} blocks
 * @property {Object<string, {type, data: {mention: PersonData, emojiUnicode:string}}>} entityMap
 */